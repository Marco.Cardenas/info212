import tkinter as tk

BACKGROUND_CLR = "#cde2b8"
PLACEHOLDER_CLR = "#dddddd"
CLR_LIST =["purple","yellow","red","cyan","orange"]

class Game(tk.Frame):
    def __init__(self):
        tk.Frame.__init__(self)
        self.grid()
        self.master.title("Stigespill")

        self.main_grid = tk.Frame(
            self,
            bg = "black",
            bd = 3,
            width = 800,
            height = 600
        )
        self.main_grid.grid(pady=(100, 0))
        self.make_GUI()
        self.mainloop()

    def make_GUI(self):
        # Make grid
        self.cells = []
        for i in range(10):
            row = []
            for j in range(10):
                cell_frame = tk.Frame(
                    self.main_grid,
                    bg = CLR_LIST[(i+j)%5],
                    width=50,
                    height=50,
                )
                # Make the code underneath into a function
                if i % 2 == 0:
                    pos_j = 9-j
                else:
                    pos_j = j
                pos_i = 9-i
                pos_x = int(str(pos_i)+str(pos_j))
                cell_frame.grid(row=i, column=j, padx=1, pady=1)
                cell_number = tk.Label(self.main_grid, bg=CLR_LIST[(i+j) % 5], text=pos_x)
                cell_number.grid(row=i, column=j)
                cell_data = {"frame": cell_frame, "number": cell_number}
                row.append(cell_data)
            self.cells.append(row)

        title = tk.Frame(self)
        title.place(relx=0.5, y=45, anchor="center")
        tk.Label(
            title,
            text = "Stigespill",
            font = "Courier"
        ).grid(row = 0)

Game()

