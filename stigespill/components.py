import random


class Square:
    def __init__(self, id_nr):
        self.id_nr = id_nr
        self.persons = []


class Players:
    def __init__(self, color):
        self.color = color
        self.pos = 0

    def move(self, moves):
        self.pos += moves

    def get_pos(self):
        return self.pos

    def get_color(self):
        return self.color


class Dice:
    def __init__(self, sides):
        self.sides = sides

    def roll(self):
        eyes = random.randint(1, self.sides)
        return eyes


class Board:
    def __init__(self, squares):
        self.squares = {x: Square(x) for x in range(squares)}


class Ladder:
    def __init__(self, start, end):
        self.start = start
        self.end = end

    def move_player(self, player):
        jump = self.end - self.start
        player.move(jump)
